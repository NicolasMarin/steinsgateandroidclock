# SteinsGateAndroidClock [![build status](https://gitlab.com/NicolasMarin/steinsgateandroidclock/badges/master/pipeline.svg?job=assembleDebug&key_text=Build)](https://gitlab.com/NicolasMarin/steinsgateandroidclock/-/commits/master) [![test status](https://gitlab.com/NicolasMarin/steinsgateandroidclock/badges/master/pipeline.svg?job=debugTests&key_text=Test)](https://gitlab.com/NicolasMarin/steinsgateandroidclock/-/commits/master)

Android App that works as a Clock, made in Java following the MVP pattern, with the style based on the Divergence Meter from the Steins;Gate anime.


## Features

The application has the following features:

<img src="images/menu.png" width="350" alt="Screenshot de la Aplicacion"/>

### Clock

<img src="images/clock.png" width="350" alt="Screenshot de la Aplicacion"/>

### Timer

<p float="left">
  <img src="images/timer.png" width="350" alt="Screenshot de la Aplicacion"/>

  <img src="images/keyboard with values.png" width="350" alt="Screenshot de la Aplicacion"/>
</p>
<p float="left">
  <img src="images/timer with values 1.png" width="350" alt="Screenshot de la Aplicacion"/>

  <img src="images/timer with values 2.png" width="350" alt="Screenshot de la Aplicacion"/>
</p>

### Chronometer

<p float="left">
  <img src="images/chronometer 0.png" width="350" alt="Screenshot de la Aplicacion"/>

  <img src="images/chronometer 1.png" width="350" alt="Screenshot de la Aplicacion"/>
</p>