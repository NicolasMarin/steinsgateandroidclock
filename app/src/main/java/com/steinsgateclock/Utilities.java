package com.steinsgateclock;

import android.app.Activity;
import android.widget.ImageView;

public class Utilities {

    public static void updateImageView(Activity activity, int imageViewId, int value) {
        ImageView img = activity.findViewById(imageViewId);
        if(img != null && value != -1){
            img.setImageResource(getImageId(value));
            img.setMaxHeight(10);
            img.setMaxWidth(4);
        }
    }

    private static int getImageId(int value) {
        int[] allDigitsIds = {R.drawable.number_0, R.drawable.number_1, R.drawable.number_2, R.drawable.number_3, R.drawable.number_4,
                R.drawable.number_5, R.drawable.number_6, R.drawable.number_7, R.drawable.number_8, R.drawable.number_9};
        return (value > -1 && value < allDigitsIds.length) ? allDigitsIds[value] : allDigitsIds[0];
    }
}
