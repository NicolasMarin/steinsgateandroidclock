package com.steinsgateclock.model;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.model.pojo.DoubleDigit;
import com.steinsgateclock.model.pojo.FourDigit;

import java.util.Calendar;

public class ClockManager extends DigitsManager{
    private DoubleDigit day, month;
    private FourDigit year;

    public ClockManager() { super(); }

    @Override
    public void updateAllDigits(Calendar cal, Contract.PresenterTimeRunning presenter){
        getNumber   (cal.get(Calendar.DATE), 0);
        getNumber   (cal.get(Calendar.MONTH) + 1, 1);
        getYear     (cal.get(Calendar.YEAR));

        getNumber   (cal.get(Calendar.HOUR_OF_DAY), 2);
        getNumber   (cal.get(Calendar.MINUTE), 3);
        getNumber   (cal.get(Calendar.SECOND), 4);
        presenter.updateTimeRunning();
    }

    private void getYear(int yearNumber){
        year = new FourDigit(yearNumber);
    }

    private void getNumber(int number, int index){
        int digit1, digit2;
        String[] digitsArray = String.valueOf(number).split("");
        if(digitsArray.length > 1){
            digit1 = Integer.parseInt(digitsArray[0]);
            digit2 = Integer.parseInt(digitsArray[1]);
        }else{
            digit1 = 0;
            digit2 = Integer.parseInt(digitsArray[0]);
        }
        DoubleDigit doubleDigit = new DoubleDigit(digit1, digit2);
        switch (index){
            case 0 : day    = doubleDigit; break;
            case 1 : month  = doubleDigit; break;
            case 2 : digits.setHour(doubleDigit); break;
            case 3 : digits.setMinute(doubleDigit); break;
            case 4 : digits.setSecond(doubleDigit); break;
        }
    }

    public int getDayDigit1() {     return day.getDigit1(); }
    public int getDayDigit2() {     return day.getDigit2(); }
    public int getMonthDigit1() {   return month.getDigit1(); }
    public int getMonthDigit2() {   return month.getDigit2(); }
    public int getYearDigit1() {    return year.getDigit1(); }
    public int getYearDigit2() {    return year.getDigit2(); }
    public int getYearDigit3() {    return year.getDigit3(); }
    public int getYearDigit4() {    return year.getDigit4(); }

    public boolean equalsDay    (int digit1, int digit2) { return day   .equalsDoubleDigit(digit1, digit2); }
    public boolean equalsMonths (int digit1, int digit2) { return month .equalsDoubleDigit(digit1, digit2); }
}
