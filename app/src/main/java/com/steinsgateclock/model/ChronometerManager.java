package com.steinsgateclock.model;

import androidx.annotation.NonNull;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.model.pojo.HourDigitsMilliseconds;
import com.steinsgateclock.model.pojo.DoubleDigit;

public class ChronometerManager extends DigitsManager{

    private HourDigitsMilliseconds chronometerDigits;

    public ChronometerManager() {
        super();
        chronometerDigits = new HourDigitsMilliseconds(digits);
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString() + "ChronometerManager{" +
                "chronometerDigits=" + chronometerDigits +
                '}';
    }

    public void updateAllDigits(long valueToAdd, Contract.PresenterTimeRunning presenterTimeRunning){
        super.updateAllDigits(valueToAdd, presenterTimeRunning);
        chronometerDigits.setHourDigits(digits);
        updateMilliSecondsDigits(difference);

        presenterTimeRunning.updateTimeRunning();
        milliseconds = milliseconds + valueToAdd;
    }

    private void updateMilliSecondsDigits(long difference) {
        if(difference > 9){
            if(difference < 100) {
                valueDoubleDigit = new DoubleDigit(0, (int) difference / 10);
            } else {
                valueDoubleDigit = new DoubleDigit((int) difference / 100, ((int) difference/10) % 10);
            }
            chronometerDigits.setMilliSecond(valueDoubleDigit);
        }
    }

    public int getMilliSecondDigit2() { return chronometerDigits.getMilliSecond2(); }
    public int getMilliSecondDigit1() { return chronometerDigits.getMilliSecond1(); }

}
