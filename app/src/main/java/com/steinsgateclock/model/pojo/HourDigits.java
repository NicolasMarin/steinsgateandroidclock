package com.steinsgateclock.model.pojo;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class HourDigits {
    protected DoubleDigit hour, minute, second;

    public HourDigits(DoubleDigit hour, DoubleDigit minute, DoubleDigit second) {
        this.hour   = hour;
        this.minute = minute;
        this.second = second;
    }

    public HourDigits() {
        DoubleDigit doubleDigit = new DoubleDigit(0,0);
        hour        = doubleDigit;
        minute      = doubleDigit;
        second      = doubleDigit;
    }

    public DoubleDigit getHour() { return hour; }
    public int getHour1() { return hour.getDigit1(); }
    public int getHour2() { return hour.getDigit2(); }
    public void setHour(DoubleDigit hour) { this.hour = hour; }

    public DoubleDigit getMinute() { return minute; }
    public int getMinute1() { return minute.getDigit1(); }
    public int getMinute2() { return minute.getDigit2(); }
    public void setMinute(DoubleDigit minute) { this.minute = minute; }

    public DoubleDigit getSecond() { return second; }
    public int getSecond1() { return second.getDigit1(); }
    public int getSecond2() { return second.getDigit2(); }
    public void setSecond(DoubleDigit second) { this.second = second; }

    @NonNull
    @Override
    public HourDigits clone(){
        HourDigits digits = new HourDigits();
        digits.setHour(hour);
        digits.setMinute(minute);
        digits.setSecond(second);
        return digits;
    }

    @NonNull
    @Override
    public String toString() { return hour + ":" + minute + ":" + second; }

    public void aggregateDigitAtEnd(int digitToAdd) {
        ArrayList<Integer> numberList = getArrayList();
        numberList.add(digitToAdd);
        numberList.remove(0);
        arrayListToHourDigits(numberList);
    }

    public void arrayListToHourDigits(ArrayList<Integer> numberList) {
        if (numberList.size() == 6){
            hour = new DoubleDigit(numberList.get(0), numberList.get(1));
            minute = new DoubleDigit(numberList.get(2), numberList.get(3));
            second = new DoubleDigit(numberList.get(4), numberList.get(5));
        }
    }

    private ArrayList<Integer> getArrayList() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(hour.getDigit1());
        list.add(hour.getDigit2());
        list.add(minute.getDigit1());
        list.add(minute.getDigit2());
        list.add(second.getDigit1());
        list.add(second.getDigit2());
        return list;
    }

    public boolean areAllZero() { return hour.areAllZero() && minute.areAllZero() && second.areAllZero(); }

    public void deleteDigitAtEnd() {
        ArrayList<Integer> numberList = getArrayList();
        numberList.add(0, 0);
        numberList.remove(numberList.size()-1);
        arrayListToHourDigits(numberList);
    }
}
