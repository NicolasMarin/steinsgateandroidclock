package com.steinsgateclock.model.pojo;

import androidx.annotation.NonNull;

public class HourDigitsMilliseconds extends HourDigits {

    public DoubleDigit milliSecond;

    public HourDigitsMilliseconds(HourDigits digits) {
        super(digits.hour, digits.minute, digits.second);
        milliSecond = new DoubleDigit(0,0);
    }

    public int getMilliSecond1() { return milliSecond.getDigit1(); }
    public int getMilliSecond2() { return milliSecond.getDigit2(); }

    public void setMilliSecond(DoubleDigit milliSecond) { this.milliSecond = milliSecond; }

    @NonNull
    @Override
    public HourDigitsMilliseconds clone(){
        HourDigits hourDigits = super.clone();
        HourDigitsMilliseconds digitsMilliseconds = (HourDigitsMilliseconds) hourDigits;
        digitsMilliseconds.setMilliSecond(milliSecond);
        return digitsMilliseconds;
    }

    @NonNull
    @Override
    public String toString() { return super.toString() + ":" + milliSecond; }

    public void setHourDigits(HourDigits digits) {
        hour   = digits.getHour();
        minute = digits.getHour();
        second = digits.getSecond();
    }
}
