package com.steinsgateclock.model.pojo;

public class FourDigit {
    private int digit1, digit2, digit3, digit4;

    public FourDigit(int completeNumber) {
        String[] digits = String.valueOf(completeNumber).split("");
        if(digits.length == 4){
            digit1 = Integer.parseInt(digits[0]);
            digit2 = Integer.parseInt(digits[1]);
            digit3 = Integer.parseInt(digits[2]);
            digit4 = Integer.parseInt(digits[3]);
        }
    }

    public int getDigit1() { return digit1; }
    public int getDigit2() { return digit2; }
    public int getDigit3() { return digit3; }
    public int getDigit4() { return digit4; }
}