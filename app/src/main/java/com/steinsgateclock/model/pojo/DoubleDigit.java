package com.steinsgateclock.model.pojo;

import androidx.annotation.NonNull;

public class DoubleDigit {
    private int digit1, digit2;

    public DoubleDigit(int digit1, int digit2) {
        this.digit1 = digit1;
        this.digit2 = digit2;
    }

    public boolean equalsDoubleDigit(int digit1, int digit2) { return this.digit1 == digit1 && this.digit2 == digit2; }

    public int getDigit1() { return digit1; }
    public int getDigit2() { return digit2; }

    public void setDigit1(int digit1) { this.digit1 = digit1; }
    public void setDigit2(int digit2) { this.digit2 = digit2; }

    @NonNull
    @Override
    public String toString() { return "" + digit1 + digit2; }

    public int getTotalValue(){ return digit1 * 10 + digit2; }

    public boolean areAllZero() { return digit1 == 0 && digit2 == 0; }
}