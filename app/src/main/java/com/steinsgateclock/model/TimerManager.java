package com.steinsgateclock.model;

import com.steinsgateclock.interfaces.Contract;

public class TimerManager extends DigitsManager{

    public void updateAllDigits(long valueToSubtract, Contract.PresenterTimeRunning presenterTimeRunning){
        super.updateAllDigits(valueToSubtract, presenterTimeRunning);

        presenterTimeRunning.updateTimeRunning();
        milliseconds = milliseconds - valueToSubtract;
    }

    @Override
    public void updateMilliseconds() {
        milliseconds = digits.getHour().getTotalValue() * millisecondsOnHour;
        milliseconds += digits.getMinute().getTotalValue() * millisecondsOnMinute;
        milliseconds += digits.getSecond().getTotalValue() * millisecondsOnSecond;
    }
}
