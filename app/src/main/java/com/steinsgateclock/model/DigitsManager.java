package com.steinsgateclock.model;

import androidx.annotation.NonNull;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.model.pojo.DoubleDigit;
import com.steinsgateclock.model.pojo.HourDigits;

import java.util.Calendar;

public class DigitsManager {
    private boolean isRunning;
    protected HourDigits digits;
    protected DoubleDigit valueDoubleDigit;
    protected long milliseconds = 0;
    protected final int maxMilliseconds = 360000000, millisecondsOnHour = 3600000, millisecondsOnMinute = 60000, millisecondsOnSecond = 1000;
    protected long difference;

    @NonNull
    @Override
    public String toString() {
        return "DigitsManager{" +
                "isRunning=" + isRunning +
                ", digits=" + digits +
                ", valueDoubleDigit=" + valueDoubleDigit +
                ", milliseconds=" + milliseconds +
                ", difference=" + difference +
                '}';
    }

    public DigitsManager() { digits = new HourDigits(); }

    public void updateAllDigits(Calendar cal, Contract.PresenterTimeRunning presenter){ }

    public void updateAllDigits(long valueToSubtract, Contract.PresenterTimeRunning presenterTimeRunning/*, TimeRunningCommunicatorUI presenterTimeRunning*/){
        difference = milliseconds;
        if(difference < 0 || difference >= maxMilliseconds){
            presenterTimeRunning.timeRunningAtTheEnd();
            return;
        }

        difference = updateDoubleDigits(difference, millisecondsOnHour);
        digits.setHour(valueDoubleDigit);

        difference = updateDoubleDigits(difference, millisecondsOnMinute);
        digits.setMinute(valueDoubleDigit);

        difference = updateDoubleDigits(difference, millisecondsOnSecond);
        digits.setSecond(valueDoubleDigit);
    }

    private long updateDoubleDigits(long difference, int millisecondsByTimeUnit) {
        if(difference >= millisecondsByTimeUnit){
            int valueInt = (int) difference / millisecondsByTimeUnit;
            difference = difference % millisecondsByTimeUnit;
            if(valueInt < 10) {
                valueDoubleDigit = new DoubleDigit(0, valueInt);
            } else {
                valueDoubleDigit = new DoubleDigit(valueInt / 10, valueInt % 10);
            }
        }else {
            valueDoubleDigit = new DoubleDigit(0, 0);
        }
        return difference;
    }

    public int getSecondDigit2() { return digits.getSecond2(); }
    public int getSecondDigit1() { return digits.getSecond1(); }
    public int getMinuteDigit2() { return digits.getMinute2(); }
    public int getMinuteDigit1() { return digits.getMinute1(); }
    public int getHourDigit2() { return digits.getHour2(); }
    public int getHourDigit1() { return digits.getHour1(); }

    public boolean isRunning() { return isRunning; }
    public void setRunning(boolean running) { isRunning = running; }

    public void updateMilliseconds() { }

    public void setDigits(HourDigits chronometerDigits) { digits = chronometerDigits; }

    public int getMilliSecondDigit1() { return -1; }
    public int getMilliSecondDigit2() { return -1; }
    public int getDayDigit1() { return -1;}
    public int getDayDigit2() { return -1;}
    public int getMonthDigit1() { return -1;}
    public int getMonthDigit2() { return -1;}
    public int getYearDigit1() { return -1;}
    public int getYearDigit2() { return -1;}
    public int getYearDigit3() { return -1;}
    public int getYearDigit4() { return -1;}

    public boolean equalsHours  (int digit1, int digit2) { return digits.getHour()  .equalsDoubleDigit(digit1, digit2); }
    public boolean equalsMinutes(int digit1, int digit2) { return digits.getMinute().equalsDoubleDigit(digit1, digit2); }
    public boolean equalsSeconds(int digit1, int digit2) { return digits.getSecond().equalsDoubleDigit(digit1, digit2); }
}
