package com.steinsgateclock.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.Lifecycle;

import com.steinsgateclock.R;
import com.steinsgateclock.model.DigitsManager;

public class FragmentClock extends FragmentWithTimeRunning {

    public static FragmentClock newInstance() { return new FragmentClock(); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_clock, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void updateDigits(DigitsManager digitsManager) {
        if(getLifecycle().getCurrentState().equals(Lifecycle.State.RESUMED)) {
            super.updateDigits(digitsManager);
        }
    }
}