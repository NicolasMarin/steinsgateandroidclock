package com.steinsgateclock.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.R;
import com.steinsgateclock.Utilities;
import com.steinsgateclock.model.pojo.HourDigits;

public class FragmentKeyboard extends FragmentBasic implements Contract.ViewKeyboard {

    private Button cancel, save, delete;
    private ImageView number1, number2, number3, number4, number5, number6, number7, number8, number9, number0;
    private Contract.PresenterKeyboard presenter;

    public static FragmentKeyboard newInstance() { return new FragmentKeyboard(); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view       = inflater.inflate(R.layout.fragment_keyboard, container, false);
        setUpButtons(view);
        setButtonsOnClickListener();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void setPresenter(@NonNull Contract.PresenterKeyboard newPresenter) {
        presenter = newPresenter;
    }

    @Override
    public void popBackStack() {
        if (getFragmentManager() != null){
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void updateDigits(HourDigits digits, boolean showSaveButton) {
        if(getLifecycle().getCurrentState().equals(Lifecycle.State.STARTED) ||
                getLifecycle().getCurrentState().equals(Lifecycle.State.RESUMED)) {
            Utilities.updateImageView(activity, R.id.secondDigit2, digits.getSecond2());
            Utilities.updateImageView(activity, R.id.secondDigit1, digits.getSecond1());

            Utilities.updateImageView(activity, R.id.minuteDigit2, digits.getMinute2());
            Utilities.updateImageView(activity, R.id.minuteDigit1, digits.getMinute1());

            Utilities.updateImageView(activity, R.id.hourDigit2, digits.getHour2());
            Utilities.updateImageView(activity, R.id.hourDigit1, digits.getHour1());

            updateSaveButton(showSaveButton);
        }
    }

    private void updateSaveButton(boolean showSaveButton) {
        if (save != null){
            save.setVisibility(showSaveButton ? View.INVISIBLE : View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void setButtonsOnClickListener() {
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { presenter.deleteAction(); }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { presenter.cancelAction(); }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { presenter.saveAction(); }
        });
        setUpNumberButton(number0, 0);
        setUpNumberButton(number1, 1);
        setUpNumberButton(number2, 2);
        setUpNumberButton(number3, 3);
        setUpNumberButton(number4, 4);
        setUpNumberButton(number5, 5);
        setUpNumberButton(number6, 6);
        setUpNumberButton(number7, 7);
        setUpNumberButton(number8, 8);
        setUpNumberButton(number9, 9);
    }

    private void setUpNumberButton(ImageView number, final int valueToAdd) {
        number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { presenter.numberAction(valueToAdd); }
        });
    }

    private void setUpButtons(View view) {
        cancel    = view.findViewById(R.id.cancel);
        save      = view.findViewById(R.id.save);
        delete    = view.findViewById(R.id.delete);
        number0   = view.findViewById(R.id.number0);
        number1   = view.findViewById(R.id.number1);
        number2   = view.findViewById(R.id.number2);
        number3   = view.findViewById(R.id.number3);
        number4   = view.findViewById(R.id.number4);
        number5   = view.findViewById(R.id.number5);
        number6   = view.findViewById(R.id.number6);
        number7   = view.findViewById(R.id.number7);
        number8   = view.findViewById(R.id.number8);
        number9   = view.findViewById(R.id.number9);
    }
}