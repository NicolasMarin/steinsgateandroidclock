package com.steinsgateclock.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.lifecycle.Lifecycle;

import com.steinsgateclock.R;
import com.steinsgateclock.model.DigitsManager;

public class FragmentChronometer extends FragmentWithTimeRunning {

    public static FragmentChronometer newInstance() { return new FragmentChronometer(); }

    public FragmentChronometer() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view       = inflater.inflate(R.layout.fragment_chronometer, container, false);
        startPause      = view.findViewById(R.id.start_pause);
        Button restart  = view.findViewById(R.id.restart);
        startPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { presenter.startPauseAction(); }
        });
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { presenter.restartAction(); }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void updateDigits(DigitsManager digitsManager) {
        if(getLifecycle().getCurrentState().equals(Lifecycle.State.STARTED) ||
                getLifecycle().getCurrentState().equals(Lifecycle.State.RESUMED)) {
            super.updateDigits(digitsManager);
        }
    }
}