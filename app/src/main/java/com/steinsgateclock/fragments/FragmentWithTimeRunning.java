package com.steinsgateclock.fragments;

import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.R;
import com.steinsgateclock.Utilities;
import com.steinsgateclock.model.DigitsManager;

public abstract class FragmentWithTimeRunning extends FragmentBasic implements Contract.ViewTimeRunning {
    protected Contract.PresenterTimeRunning presenter;
    protected Button startPause;

    @Override
    public void setPresenter(@NonNull Contract.PresenterTimeRunning newPresenter) {
        presenter = newPresenter;
    }

    @Override
    public void updateDigits(final DigitsManager digitsManager) {
        if(digitsManager != null){
            updateHourFull(digitsManager);
            updateDate(digitsManager);
            presenter.updateStartPauseButton();
        }
    }

    private void updateDate(DigitsManager digitsManager) {
        Utilities.updateImageView(activity, R.id.dayDigit1, digitsManager.getDayDigit1());
        Utilities.updateImageView(activity, R.id.dayDigit2, digitsManager.getDayDigit2());

        Utilities.updateImageView(activity, R.id.monthDigit1, digitsManager.getMonthDigit1());
        Utilities.updateImageView(activity, R.id.monthDigit2, digitsManager.getMonthDigit2());

        Utilities.updateImageView(activity, R.id.yearDigit1, digitsManager.getYearDigit1());
        Utilities.updateImageView(activity, R.id.yearDigit2, digitsManager.getYearDigit2());
        Utilities.updateImageView(activity, R.id.yearDigit3, digitsManager.getYearDigit3());
        Utilities.updateImageView(activity, R.id.yearDigit4, digitsManager.getYearDigit4());
    }

    private void updateHourFull(DigitsManager digitsManager) {
        Utilities.updateImageView(activity, R.id.hourDigit1, digitsManager.getHourDigit1());
        Utilities.updateImageView(activity, R.id.hourDigit2, digitsManager.getHourDigit2());

        Utilities.updateImageView(activity, R.id.minuteDigit1, digitsManager.getMinuteDigit1());
        Utilities.updateImageView(activity, R.id.minuteDigit2, digitsManager.getMinuteDigit2());

        Utilities.updateImageView(activity, R.id.secondDigit1, digitsManager.getSecondDigit1());
        Utilities.updateImageView(activity, R.id.secondDigit2, digitsManager.getSecondDigit2());

        Utilities.updateImageView(activity, R.id.miliSecondDigit2, digitsManager.getMilliSecondDigit2());
        Utilities.updateImageView(activity, R.id.miliSecondDigit1, digitsManager.getMilliSecondDigit1());
    }

    public void setStartPauseText(int textResource){
        if (startPause != null){
            startPause.setText(textResource);
        }
    }

    public void showMessageTimeLimit(int idResource){
        if (isResumed()) {
            Toast.makeText(activity, getString(idResource), Toast.LENGTH_SHORT).show();
        }
    }
}