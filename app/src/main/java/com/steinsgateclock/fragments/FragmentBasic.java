package com.steinsgateclock.fragments;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.MainActivity;

public class FragmentBasic extends Fragment implements Contract.BasicView {
    protected MainActivity activity;
    protected Contract.BasePresenter presenter;

    @Override
    public void onAttach(@NonNull Context context) {
        activity = (MainActivity) context;
        super.onAttach(context);
    }

    @Override
    public void setPresenter(@NonNull Contract.BasePresenter newPresenter) {
        presenter = newPresenter;
    }

    @Override
    public MainActivity getMainActivity() { return activity; }
}