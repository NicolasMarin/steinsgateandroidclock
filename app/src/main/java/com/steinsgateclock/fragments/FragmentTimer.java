package com.steinsgateclock.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.lifecycle.Lifecycle;

import com.steinsgateclock.R;
import com.steinsgateclock.model.DigitsManager;
import com.steinsgateclock.presenters.PresenterKeyboard;

public class FragmentTimer extends FragmentWithTimeRunning {

    public static FragmentTimer newInstance() { return new FragmentTimer(); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view        = inflater.inflate(R.layout.fragment_timer, container, false);
        startPause       = view.findViewById(R.id.start_pause);
        Button restart   = view.findViewById(R.id.restart);
        Button aggregate = view.findViewById(R.id.aggregate);
        startPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { presenter.startPauseAction(); }
        });
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { presenter.restartAction(); }
        });
        aggregate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { aggregateAction(); }
        });
        return view;
    }

    @Override
    public void onStart() {
        presenter.start();
        super.onStart();
    }

    private void aggregateAction() {
        FragmentKeyboard fragment = FragmentKeyboard.newInstance();
        PresenterKeyboard.newInstance(fragment);
        if(getFragmentManager() != null){
            getFragmentManager().beginTransaction().replace(R.id.container_layout, fragment)
                    .addToBackStack(fragment.getClass().getName())
                    .commit();
        }
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void updateDigits(DigitsManager digitsManager) {
        if(getLifecycle().getCurrentState().equals(Lifecycle.State.CREATED) ||
                getLifecycle().getCurrentState().equals(Lifecycle.State.RESUMED)) {
            super.updateDigits(digitsManager);
        }
    }

}