package com.steinsgateclock.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.steinsgateclock.R;

public class FragmentAlarm extends FragmentBasic {

    public static FragmentAlarm newInstance() { return new FragmentAlarm(); }

    public FragmentAlarm() { /*title = R.string.nav_alarm;*/ }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { return inflater.inflate(R.layout.fragment_alarm, container, false); }
}