package com.steinsgateclock;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.steinsgateclock.model.pojo.HourDigits;

public class PersistenceManager {

    private static String timer = "TIMER";
    private static Gson gson = new Gson();

    public static void saveTimerDigits(Context context, HourDigits digits) {
        SharedPreferences sp = context.getSharedPreferences(timer, Context.MODE_PRIVATE);
        String json = gson.toJson(digits);
        sp.edit().putString(timer, json).apply();
    }

    public static HourDigits getTimerDigits(Context context) {
        SharedPreferences sp = context.getSharedPreferences(timer, Context.MODE_PRIVATE);
        String json = sp.getString(timer, "");
        HourDigits digits = gson.fromJson(json, HourDigits.class);
        return digits != null ? digits : new HourDigits();
    }
}
