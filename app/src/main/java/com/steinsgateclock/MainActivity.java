package com.steinsgateclock;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.steinsgateclock.fragments.FragmentAlarm;
import com.steinsgateclock.fragments.FragmentBasic;
import com.steinsgateclock.fragments.FragmentChronometer;
import com.steinsgateclock.fragments.FragmentClock;
import com.steinsgateclock.fragments.FragmentTimer;
import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.interfaces.MainCommunicatorUI;
import com.steinsgateclock.model.DigitsManager;
import com.steinsgateclock.presenters.PresenterClock;
import com.steinsgateclock.presenters.PresenterChronometer;
import com.steinsgateclock.presenters.PresenterTimer;

import java.util.Objects;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, MainCommunicatorUI {

    private FragmentBasic[] fragments = {FragmentClock.newInstance(), FragmentAlarm.newInstance(),
                                        FragmentTimer.newInstance(), FragmentChronometer.newInstance()};
    private Contract.PresenterFeature[] presenters = { PresenterClock.newInstance((Contract.ViewTimeRunning) fragments[0]),
                                                PresenterTimer.newInstance((Contract.ViewTimeRunning) fragments[2]),
                                                PresenterTimer.newInstance((Contract.ViewTimeRunning) fragments[2]),
                                                PresenterChronometer.newInstance((Contract.ViewTimeRunning) fragments[3])};
    private DrawerLayout drawerLayout;
    private TextView titleTextView;
    private DigitsManager chronometerManager, timerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        titleTextView = toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        titleTextView.setText(toolbar.getTitle());
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        MenuItem menuItem = navigationView.getMenu().getItem(0);
        onNavigationItemSelected(menuItem);
        menuItem.setChecked(true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        FragmentBasic fragment;
        Contract.PresenterFeature presenter;
        switch (menuItem.getItemId()) {
            case R.id.nav_clock:            fragment  = fragments[0];
                                            presenter = presenters[0]; break;
            //case R.id.nav_alarm:            fragment = fragments[1]; break;
            case R.id.nav_timer:            fragment = fragments[2];
                                            presenter = presenters[2]; break;
            case R.id.nav_chronometer:      fragment = fragments[3];
                                            presenter = presenters[3]; break;
            /*case R.id.nav_alarm:
            case R.id.nav_widget:
                Toast.makeText(this, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;*/
            default:
                throw new IllegalArgumentException("menu option not implemented!!");
        }

        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack();
        fm.beginTransaction().replace(R.id.container_layout, fragment).commit();

        titleTextView.setText(getString(presenter.getTitle()));
        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void setChronometerManager(DigitsManager chronometerManager) { this.chronometerManager = chronometerManager; }

    @Override
    public DigitsManager getChronometerManager() { return chronometerManager; }

    @Override
    public void setTimerManager(DigitsManager timerManager) { this.timerManager = timerManager; }

    @Override
    public DigitsManager getTimerManager() { return timerManager; }
}
