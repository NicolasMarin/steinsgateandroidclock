package com.steinsgateclock.presenters;

import androidx.annotation.NonNull;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.R;

public class PresenterAlarm extends PresenterWithTimeRunning implements Contract.PresenterFeature {

    public PresenterAlarm(@NonNull Contract.ViewTimeRunning view) { super(view); }

    @Override
    protected void loadManager() { }

    @Override
    protected void loadManagerOnRestart() { }

    public static Contract.PresenterFeature newInstance(Contract.ViewTimeRunning fragment) { return new PresenterAlarm(fragment); }

    @Override
    public int getTitle() { return R.string.nav_clock; }

    @Override
    public void onDestroy() { }
}
