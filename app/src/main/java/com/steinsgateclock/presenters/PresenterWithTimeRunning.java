package com.steinsgateclock.presenters;

import androidx.annotation.NonNull;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.model.DigitsManager;
import com.steinsgateclock.R;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class PresenterWithTimeRunning implements Contract.PresenterTimeRunning {
    protected final Contract.ViewTimeRunning view;
    protected ScheduledExecutorService scheduledExecutorService;
    protected DigitsManager digitsManager;
    protected int millisecondBetweenRepetition = 0;
    protected Contract.PresenterTimeRunning presenterTimeRunning = this;

    public PresenterWithTimeRunning(@NonNull Contract.ViewTimeRunning newView) {
        view = newView;
        view.setPresenter(this);
    }

    @Override
    public void start() {
        loadManager();
        updateTimeRunning();
    }

    protected abstract void loadManager();

    protected abstract void loadManagerOnRestart();

    protected void startTimeRunning(){
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                view.getMainActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (digitsManager.isRunning()){
                            digitsManager.updateAllDigits(millisecondBetweenRepetition, presenterTimeRunning);
                        }
                    }
                });
            }
        }, 0, millisecondBetweenRepetition, TimeUnit.MILLISECONDS);
    }

    @Override
    public void updateTimeRunning() { view.updateDigits(digitsManager); }

    public void restartAction() {
        if(digitsManager.isRunning()){
            digitsManager.setRunning(false);
            stopTimeRunningAction();
        }
        loadManagerOnRestart();
        updateTimeRunning();
    }

    public void startPauseAction() {
        if(digitsManager.isRunning()){
            stopTimeRunningAction();
        }else {
            startTimeRunning();
        }
        digitsManager.setRunning(!digitsManager.isRunning());
        updateStartPauseButton();
    }

    public void updateStartPauseButton() {
        if(digitsManager != null){
            view.setStartPauseText(digitsManager.isRunning() ? R.string.pause : R.string.start);
        }
    }

    protected void stopTimeRunningAction() { scheduledExecutorService.shutdown(); }

    public void timeRunningAtTheEnd() {
        startPauseAction();
        view.showMessageTimeLimit(R.string.time_limit);
    }
}
