package com.steinsgateclock.presenters;

import androidx.annotation.NonNull;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.PersistenceManager;
import com.steinsgateclock.R;
import com.steinsgateclock.model.TimerManager;

import java.util.Objects;

public class PresenterTimer extends PresenterWithTimeRunning implements Contract.PresenterFeature {

    public PresenterTimer(@NonNull Contract.ViewTimeRunning view) { super(view); }

    public static Contract.PresenterFeature newInstance(Contract.ViewTimeRunning view) { return new PresenterTimer(view); }

    @Override
    public void start() {
        super.start();
        millisecondBetweenRepetition = 500;
    }

    @Override
    protected void startTimeRunning() {
        if(digitsManager == null){
            digitsManager = new TimerManager();
        }
        digitsManager.updateMilliseconds();
        super.startTimeRunning();
    }

    @Override
    protected void loadManagerOnRestart() { loadManager(); }

    @Override
    protected void loadManager() {
        digitsManager = view.getMainActivity().getTimerManager() != null ? view.getMainActivity().getTimerManager() : new TimerManager();
        digitsManager.setDigits(PersistenceManager.getTimerDigits(Objects.requireNonNull(view.getMainActivity())));
    }

    @Override
    public int getTitle() { return R.string.nav_timer; }

    @Override
    public void onDestroy() { view.getMainActivity().setTimerManager(digitsManager); }
}
