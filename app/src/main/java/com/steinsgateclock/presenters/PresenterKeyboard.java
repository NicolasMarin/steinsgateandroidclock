package com.steinsgateclock.presenters;

import androidx.annotation.NonNull;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.PersistenceManager;
import com.steinsgateclock.model.pojo.HourDigits;

import java.util.Objects;

public class PresenterKeyboard implements Contract.PresenterKeyboard {

    Contract.ViewKeyboard view;
    private HourDigits digits;

    public PresenterKeyboard(@NonNull Contract.ViewKeyboard newView) {
        view = newView;
        view.setPresenter(this);
        digits = new HourDigits();
    }

    public static Contract.PresenterKeyboard newInstance(Contract.ViewKeyboard view) { return new PresenterKeyboard(view); }

    @Override
    public void start() { loadDigits(); }

    @Override
    public void onDestroy() { }

    private void loadDigits() {
        digits = PersistenceManager.getTimerDigits(Objects.requireNonNull(view.getMainActivity()));
        view.updateDigits(digits, digits.areAllZero());
    }

    @Override
    public void deleteAction() {
        if(!digits.areAllZero()){
            digits.deleteDigitAtEnd();
            view.updateDigits(digits, digits.areAllZero());
        }
    }

    @Override
    public void cancelAction() {
        view.popBackStack();
    }

    @Override
    public void saveAction() {
        PersistenceManager.saveTimerDigits(Objects.requireNonNull(view.getMainActivity()), digits);
        view.popBackStack();
    }

    @Override
    public void numberAction(int valueToAdd) {
        if (digits.getHour1() == 0) {
            digits.aggregateDigitAtEnd(valueToAdd);
            view.updateDigits(digits, digits.areAllZero());
        }
    }
}
