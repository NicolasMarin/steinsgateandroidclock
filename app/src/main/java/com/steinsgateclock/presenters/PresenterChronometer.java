package com.steinsgateclock.presenters;

import androidx.annotation.NonNull;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.R;
import com.steinsgateclock.model.ChronometerManager;

public class PresenterChronometer extends PresenterWithTimeRunning implements Contract.PresenterFeature {

    public PresenterChronometer(@NonNull Contract.ViewTimeRunning view) { super(view); }

    public static Contract.PresenterFeature newInstance(Contract.ViewTimeRunning view) { return new PresenterChronometer(view); }

    @Override
    public void start() {
        super.start();
        millisecondBetweenRepetition = 10;
    }

    @Override
    protected void startTimeRunning() {
        if(digitsManager == null){
            digitsManager = new ChronometerManager();
        }
        super.startTimeRunning();
    }

    @Override
    protected void loadManagerOnRestart() { digitsManager = new ChronometerManager(); }

    @Override
    protected void loadManager() {
        digitsManager = view.getMainActivity().getChronometerManager() != null ? view.getMainActivity().getChronometerManager() : new ChronometerManager();
    }

    @Override
    public int getTitle() { return R.string.nav_chronometer; }

    @Override
    public void onDestroy() { view.getMainActivity().setChronometerManager(digitsManager); }
}
