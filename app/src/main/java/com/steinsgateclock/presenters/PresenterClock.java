package com.steinsgateclock.presenters;

import androidx.annotation.NonNull;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.model.ClockManager;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import com.steinsgateclock.R;

public class PresenterClock extends PresenterWithTimeRunning implements Contract.PresenterFeature {

    public PresenterClock(@NonNull Contract.ViewTimeRunning view) { super(view); }

    public static Contract.PresenterFeature newInstance(Contract.ViewTimeRunning fragment) { return new PresenterClock(fragment); }

    @Override
    public void start() {
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        digitsManager = new ClockManager();
        startTimeRunning();
    }

    @Override
    protected void loadManager() { }

    @Override
    protected void loadManagerOnRestart() { }

    @Override
    public void startTimeRunning() {
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                final Calendar cal = Calendar.getInstance();
                digitsManager.updateAllDigits(cal, presenterTimeRunning);
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

    @Override
    public int getTitle() { return R.string.nav_clock; }

    @Override
    public void onDestroy() { }
}
