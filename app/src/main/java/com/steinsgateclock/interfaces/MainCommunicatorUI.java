package com.steinsgateclock.interfaces;

import com.steinsgateclock.model.DigitsManager;

public interface MainCommunicatorUI {

    void setChronometerManager(DigitsManager chronometerManager);

    DigitsManager getChronometerManager();

    void setTimerManager(DigitsManager timerManager);

    DigitsManager getTimerManager();
}
