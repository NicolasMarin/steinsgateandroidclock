package com.steinsgateclock.interfaces;

import androidx.annotation.NonNull;

import com.steinsgateclock.MainActivity;
import com.steinsgateclock.model.DigitsManager;
import com.steinsgateclock.model.pojo.HourDigits;

public interface Contract {

    interface BasePresenter {
        void start();
        void onDestroy();
    }

    interface PresenterFeature extends BasePresenter{
        int getTitle();
    }

    interface PresenterTimeRunning extends PresenterFeature {
        void updateTimeRunning();
        void startPauseAction();
        void restartAction();
        void timeRunningAtTheEnd();
        void updateStartPauseButton();
    }

    interface PresenterKeyboard extends BasePresenter{
        void deleteAction();
        void cancelAction();
        void saveAction();
        void numberAction(int valueToAdd);
    }




    interface BasicView {
        void setPresenter(BasePresenter presenter);
        MainActivity getMainActivity();
    }

    interface ViewTimeRunning extends BasicView {
        void setPresenter(PresenterTimeRunning presenter);
        void updateDigits(DigitsManager digitsManager);
        void setStartPauseText(int textResource);
        void showMessageTimeLimit(int time_limit);
    }

    interface ViewKeyboard extends BasicView{
        void updateDigits(HourDigits digits, boolean showSaveButton);
        void setPresenter(@NonNull Contract.PresenterKeyboard newPresenter);
        void popBackStack();
    }




}
