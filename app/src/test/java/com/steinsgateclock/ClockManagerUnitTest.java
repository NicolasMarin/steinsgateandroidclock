package com.steinsgateclock;

import com.steinsgateclock.interfaces.Contract;
import com.steinsgateclock.model.ClockManager;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

public class ClockManagerUnitTest {

    private ClockManager clockManager;
    private Calendar cal;
    private Contract.PresenterTimeRunning presenterTimeRunning;

    @Before
    public void setUp() {
        presenterTimeRunning = new PresenterTimeRunningImplementation();
        clockManager        = new ClockManager();
        cal                 = Calendar.getInstance();
        cal.set(2020, 0, 1, 17, 8, 30);
    }


    //TESTING MONTHS
    @Test
    public void test_2020_01_01_17_08_30() {
        prepareTestMonth(0);
        assertTrue(clockManager.equalsMonths(0, 1));
    }

    @Test
    public void test_2020_05_01_17_08_30() {
        prepareTestMonth(4);
        assertTrue(clockManager.equalsMonths(0, 5));
    }

    @Test
    public void test_2020_12_01_17_08_30() {
        prepareTestMonth(11);
        assertTrue(clockManager.equalsMonths(1, 2));
    }

    private void prepareTestMonth(int value) { prepareTest(Calendar.MONTH, value); }
    private void prepareTestDate(int value) { prepareTest(Calendar.DATE, value); }
    private void prepareTestHour(int value) { prepareTest(Calendar.HOUR_OF_DAY, value); }
    private void prepareTestMinute(int value) { prepareTest(Calendar.MINUTE, value); }
    private void prepareTestSecond(int value) { prepareTest(Calendar.SECOND, value); }

    private void prepareTest(int key, int value) {
        cal.set(key, value);
        clockManager.updateAllDigits(cal, presenterTimeRunning);
    }


    //TESTING DATES
    @Test
    public void test_2020_10_01_17_08_30() {
        prepareTestDate(1);
        assertTrue(clockManager.equalsDay(0, 1));
    }

    @Test
    public void test_2020_10_13_17_08_30() {
        prepareTestDate(13);
        assertTrue(clockManager.equalsDay(1, 3));
    }

    @Test
    public void test_2020_10_23_17_08_30() {
        prepareTestDate(23);
        assertTrue(clockManager.equalsDay(2, 3));
    }

    @Test
    public void test_2020_10_31_17_08_30() {
        prepareTestDate(31);
        assertTrue(clockManager.equalsDay(3, 1));
    }


    //TESTING HOURS
    @Test
    public void test_2020_10_01_00_08_30() {
        prepareTestHour(0);
        assertTrue(clockManager.equalsHours(0, 0));
    }

    @Test
    public void test_2020_10_01_01_08_30() {
        prepareTestHour(1);
        assertTrue(clockManager.equalsHours(0, 1));
    }

    @Test
    public void test_2020_10_01_09_08_30() {
        prepareTestHour(9);
        assertTrue(clockManager.equalsHours(0, 9));
    }

    @Test
    public void test_2020_10_01_10_08_30() {
        prepareTestHour(10);
        assertTrue(clockManager.equalsHours(1, 0));
    }

    @Test
    public void test_2020_10_01_12_08_30() {
        prepareTestHour(12);
        assertTrue(clockManager.equalsHours(1, 2));
    }

    @Test
    public void test_2020_10_01_15_08_30() {
        prepareTestHour(15);
        assertTrue(clockManager.equalsHours(1, 5));
    }

    @Test
    public void test_2020_10_01_20_08_30() {
        prepareTestHour(20);
        assertTrue(clockManager.equalsHours(2, 0));
    }

    @Test
    public void test_2020_10_01_23_08_30() {
        prepareTestHour(23);
        assertTrue(clockManager.equalsHours(2, 3));
    }


    //TESTING MINUTES
    @Test
    public void test_2020_10_01_01_00_30() {
        prepareTestMinute(0);
        assertTrue(clockManager.equalsMinutes(0, 0));
    }

    @Test
    public void test_2020_10_01_01_05_30() {
        prepareTestMinute(5);
        assertTrue(clockManager.equalsMinutes(0, 5));
    }

    @Test
    public void test_2020_10_01_01_10_30() {
        prepareTestMinute(10);
        assertTrue(clockManager.equalsMinutes(1, 0));
    }

    @Test
    public void test_2020_10_01_01_17_30() {
        prepareTestMinute(17);
        assertTrue(clockManager.equalsMinutes(1, 7));
    }

    @Test
    public void test_2020_10_01_01_25_30() {
        prepareTestMinute(25);
        assertTrue(clockManager.equalsMinutes(2, 5));
    }

    @Test
    public void test_2020_10_01_01_34_30() {
        prepareTestMinute(34);
        assertTrue(clockManager.equalsMinutes(3, 4));
    }

    @Test
    public void test_2020_10_01_01_47_30() {
        prepareTestMinute(47);
        assertTrue(clockManager.equalsMinutes(4, 7));
    }

    @Test
    public void test_2020_10_01_01_59_30() {
        prepareTestMinute(59);
        assertTrue(clockManager.equalsMinutes(5, 9));
    }


    //TESTING SECONDS
    @Test
    public void test_2020_10_01_01_05_00() {
        prepareTestSecond(0);
        assertTrue(clockManager.equalsSeconds(0, 0));
    }

    @Test
    public void test_2020_10_01_01_05_05() {
        prepareTestSecond(5);
        assertTrue(clockManager.equalsSeconds(0, 5));
    }

    @Test
    public void test_2020_10_01_01_05_10() {
        prepareTestSecond(10);
        assertTrue(clockManager.equalsSeconds(1, 0));
    }

    @Test
    public void test_2020_10_01_01_05_17() {
        prepareTestSecond(17);
        assertTrue(clockManager.equalsSeconds(1, 7));
    }

    @Test
    public void test_2020_10_01_01_05_25() {
        prepareTestSecond(25);
        assertTrue(clockManager.equalsSeconds(2, 5));
    }

    @Test
    public void test_2020_10_01_01_05_34() {
        prepareTestSecond(34);
        assertTrue(clockManager.equalsSeconds(3, 4));
    }

    @Test
    public void test_2020_10_01_01_05_47() {
        prepareTestSecond(47);
        assertTrue(clockManager.equalsSeconds(4, 7));
    }

    @Test
    public void test_2020_10_01_01_05_59() {
        prepareTestSecond(59);
        assertTrue(clockManager.equalsSeconds(5, 9));
    }
}
