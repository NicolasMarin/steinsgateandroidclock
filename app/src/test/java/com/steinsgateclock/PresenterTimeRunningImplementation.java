package com.steinsgateclock;

import com.steinsgateclock.interfaces.Contract;

public class PresenterTimeRunningImplementation implements Contract.PresenterTimeRunning {
    @Override
    public void start() { }

    @Override
    public void onDestroy() { }

    @Override
    public int getTitle() { return 0; }

    @Override
    public void updateTimeRunning() { }

    @Override
    public void startPauseAction() { }

    @Override
    public void restartAction() { }

    @Override
    public void timeRunningAtTheEnd() { }

    @Override
    public void updateStartPauseButton() { }
}
